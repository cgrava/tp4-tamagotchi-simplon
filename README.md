# Tamagotchi-Pirate

![Pirates alt](https://thumb.canalplus.pro/http/unsafe/1280x720/filters:quality(80)/img-hapi.canalplus.pro/ServiceImage/ImageID/52836597)

L’objectif est de réaliser une page Web dynamique contenant un petit compagnon virtuel, à l’instar du fameux Tamagotchi (version pirate).

## Pour commencer

Aucune maquette graphique n'a été définie pour cette page, j'ai carte blanche.

Il s’agit d’un travail individuel.

### Pré-requis

- La page doit être responsive
- Utilisation de SASS pour la création du CSS
- Utilisation des librairies CSS possible, sauf pour le JS (jQuery interdit)

#### Version 1

- Le compagnon doit avoir au minimum 3 besoins (bagarre, pillage, navigation)
- Ces besoins vont diminuer progressivement dans le temps
- Les besoins doivent être visible à tout moment par l’utilisateur
- Quand les 3 besoins tombent à zéro, la partie est perdue
- Chaque besoin peut être alimenté en cliquant sur des boutons correspondants (sabres, trésor, etc …) 
- Les besoins ne peuvent pas dépasser un maximum
- En fonction du changement d’état des besoins, faire varier les différents éléments de la page
- Un texte d’aide rappellera les différentes règles du jeu

#### Version 2

- les besoins seront remplis par la réponse à une commande écrite, comme dans un terminal de commande
- Mettre en place un champ d’input, pour entrer les commandes, et d’un output pour afficher les résultats et les infos
- Il faut une commande pour chaque besoin et une commande d’aide pour donner les commandes disponibles
- Une commande invalide doit être indiquée à l’utilisateur


### Livrables


Un dépot Gitlab contenant le code source (HTML, SASS, CSS, Images, ...), ainsi que le schéma de la maquette graphique. Ce schéma doit être au format image (JPG, PNG) et peut être fait soit sur un outil numérique ou alors juste en mode papier/crayon. 


## Fabriqué avec

* [JavaScript](https://developer.mozilla.org/fr/docs/Web/JavaScript) - Langage de script léger, orienté objet
* [Bootstrap](https://getbootstrap.com/) - Framework CSS (front-end)
* [VisualStudioCode](https://code.visualstudio.com/) - Editeur de textes


## Auteur

* **Christophe Grava** 

